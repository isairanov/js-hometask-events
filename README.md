# js-hometask-events

## Ответьте на вопросы

### 1. Что такое событие? Какими способами можно назначить обработчик события на элемент?
> Ответ: Событие — это сигнал, который вы можете принять, обработать и выдать нужный результат. События могут приходить как из браузера (клик мышкой, фокус на элементе, окончание загрузки), так и вы сами можете генерировать события.
```html
// Первый способ с использованием HTML атрибута:
<script>
  function onCLick() {
    alert('Клик')
  }
</script>
<button value="Клик" onclick="onClick()" />
```
```html
// Второй способ с использованием свойства DOM - объекта:
<button id="btn" value="Клик" />
<script>
  btn.onclick = function() {
    alert( 'Клик!' );
  };
</script>
```
```html
// Третий способ. addEventListener: (в отличие от предыдущих можно навесить несколько событий одновременно)
<button id="btn" value="Жмак"/>

<script>
  function handler1() {
    alert('Раз жмак');
  };

  function handler2() {
    alert('Два жмак');
  }

  btn.onclick = function() { alert('Нулевой жмак') }
  btn.addEventListener("click", handler1); //Раз жмак
  btn.addEventListener("click", handler2); //Два жмак
</script>
```
### 2. Что такое всплытие? Каким образом его можно остановить?
> Ответ: Всплытие — это когда при наступлении события обработчик сначала срабатывает на самом элементе, затем на его родителе, потом на родителе родителя и так по цепочке вверх. Прекратить всплытие можно при помощи любого обработчика, который перехватывает это событие, при помощи метода event.stopPropagation(), если нужно не выполнять только родительские элементы. Если нужно остановить и на текущем элементе, то нужно использовать метод event.stopImmediatePropagation(). Он не только предотвращает всплытие, но и останавливает обработку событий на текущем элементе.
```html
<div id="grandpa">
  <p id="father">
    <span id="son">SON</span>
  </p>
</div>
<script>
function onClick(event) {
  event.stopPropagation() // или event.stopImmediatePropagation()
  alert('Event!')
}
  grandpa.onclick = onClick
  father.onclick = onClick
  son.onclick = onClick
</script>
```

### 3. Каким образом можно получить детальную информацию о событии? Чем отличаются event.target и event.currentTarget?
> Ответ: У события есть свойства. Например event.type - тип события. Чтобы узнать, на каком именно элементе сработало событие нужно воспользоваться свойством события event.target. Выше уже рассматривалось свойство event.currentTarget. Отличие этих двух свойств заключается в том, что event.currentTarget — это элемент, на котором сработал обработчик, т.е. элемент, где мы перехватили событие. currentTarget будет меняться в зависимости от того, на каком элементе сработал обработчик. А event.target — это исходный элемент, на котором произошло событие, в процессе всплытия он неизменен.

### 4. Приведите пример действия браузера по умолчанию. Каким образом действие можно отменить?
> Ответ: При нажатие на checkbox устанавливается флажок. Код ниже блокирует эту возможность:
```js
document.querySelector("#id-checkbox").addEventListener("click", function (event) {
  event.preventDefault();
});
```
```html
<form>
    <label for="id-checkbox">Checkbox:</label>
    <input type="checkbox" id="id-checkbox" />
  </form>
```


## Выполните задания

* Допишите функции в tasks.js;
* Для проверки работы функций откройте index.html в браузере;
* Создайте Merge Request с решением.
