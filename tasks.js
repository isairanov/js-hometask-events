'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/

const menuBtn = document.getElementById("menuBtn").addEventListener("click", showMenu);
const menuDiv = document.getElementById("menu").addEventListener('click', showMenu);

function showMenu() {
  const menu = document.getElementById("menu");
  menu.classList.toggle("menu-opened");
}

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/

document.getElementById("field").addEventListener("click", function (event) {
  const block = document.getElementById("movedBlock");
  const coords = this.getBoundingClientRect();
  block.style.left = event.clientX - coords.left + "px";
  block.style.top = event.clientY - coords.top + "px";
})

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/
messager.onclick = function (event) {
  const { target } = event;
  if (target.classList.contains("remove")) {
    target.parentElement.style.display = "none";
  }
}

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/
document.getElementById("links").addEventListener('click', confirmRedirect);

function confirmRedirect(event) {
  const { target } = event;
  if (target.href !== undefined) {
    if (confirm("Перейти по ссылке?")) {
      return;
    }
  }
  event.preventDefault();
}

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/

document.getElementById("fieldHeader").addEventListener("input", function (event) {
  const header = document.getElementById("taskHeader");
  header.textContent = this.value;
})